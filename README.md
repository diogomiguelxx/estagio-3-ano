
# Nominatim with CTT

This project was made by me during an internship on Ad Evolutio, Lda
with the main objective to automate a process needed for nominatim database
customization. This automatization was made with the help of an exclusive
(and paid for) CTT postcode database, and, as so, can only be aplied to
databases with the same struture as this one.

## Instalation

To make it run, all you need to do is to drop all these files into
your nominatim-planet folder (or other name for the folder you have nominatim
installed on)...

```bash
  cd nominatim-installation-folder
```
 ... and paste/download these 3 .py files.
    


## Using

First, make sure your bash user is the one you have permissions on your databases (nominatim Database and CTT Database),
otherwise you might get an error on connection with the said DB.
Import.py, Script.py and csvGeneration.py, all three scripts use command line arguments, in the following order, 
to get the information needed to get the resources necessary for their computation:
```bash
  python3 import.py DBName SqlUserName SqlUserPassword
```
But you must follow an order for the scripts to follow properly:

    1. Run import.py 

```bash
  python3 import.py CTTDatabaseName DbUser DbUserPassword
```

This first script will generate the CSV with the information
needed for importing the new Postcodes and import it to the nominatim database.
If you look at line 20 of the source code, you will see the script will generate and .csv file named
'pt_postcodes'. This is because this was created with the sole objective to get Postcodes for the Portuguese region.
If you are using an nominatim installation from any other region of the world, you should replace the 'pt' before the '_' with 
the 2 digit code of your chosen country (eg.: United Kingdom -> uk, United States -> us, ...). 
Here's the Link to the nominatim documentation about 2 digit codes:
https://wiki.openstreetmap.org/wiki/Nominatim/Country_Codes

    2. Run csvGeneration.py 

```bash
  python3 csvGeneration.py NominatimDatabaseName DbUser DbUserPassword
```
  Now, since the already have the new postcodes imported and correctly indexed,
  we will generate the .csv file with the resources needed for the final import.
  
    3. Run script.py 

```bash
  python3 script.py NominatimDatabaseName DbUser DbUserPassword
```
  Finally, the last script will import all the data from the csv generated from the
  last script, and import it to the nominatim database, while also giving it customized types and 
  giving them top priotity in the nominatim ranking system:
  https://nominatim.org/release-docs/develop/customize/Ranking/
  Due to some Open Street Map and Wikipedia data conflicts, some postcodes
  were not able to appear in the first page (altough on top of their resspective page)
  If you wish to take a look at all the data imported, you can use psql and search for all the data in 
  table placex where type is like 'postcodePoint'.


## Autors

- [@diogomiguelxx](https://gitlab.com/diogomiguelxx)

