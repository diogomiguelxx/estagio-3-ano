from multiprocessing.connection import wait
import sys
import pandas as pd
from psycopg2 import *
import os


def searchRankUpdate(curr, conn):
    sql_query = "UPDATE placex SET (rank_search, rank_address) = (1,1) WHERE type LIKE 'postcodePoint';"
    curr.execute(sql_query)
    conn.commit()
    print("New entries updated to be first of any search in nominatim")

def insertScript():
    try: 
        print("Connecting to the PostgreSQL database...")
        conn = connect(connString)

        file = pd.read_csv('search_data.csv', encoding='latin9')
        totalEntries = len(file.index)
        percentagePrintFlag = round(totalEntries/100)
        print("Total entries =", totalEntries)
        #print("PercentageFlag = ", percentagePrintFlag)
        curr = conn.cursor() 
        # number of new entries
        i = 0
        j = 0
        
        # inserting csv rows into nominatim db
        for ind in file.index:
            nome_completo = file['nome_completo'][ind]
            nome_completo = nome_completo.replace("'", "").replace('"', "")
            centroid = file['centroid'][ind]
            cp = file['cp'][ind]
            #print("nome_completo =", nome_completo)
            #print("centroid =", centroid)
            #print("cp =", cp)
            #print(file['nome_completo'][ind], file['centroid'][ind], file['cp'][ind])
            sql_query = f"INSERT INTO placex (place_id, osm_id, osm_type, class, type ,rank_search, rank_address, name, address, postcode, geometry) VALUES (1, 1, 'N', 'postcodePoint', 'postcodePoint', 1, 1,'name=>\"{nome_completo}\"', '\"postcode\"=>\"{cp}\", \"name\"=>\"{nome_completo}\"', '{cp}', '{centroid}');"
            #print(sql_query)
            curr.execute(sql_query)
            #print("Query Executada")
            sql_query = ''
            conn.commit()
            i += 1
            #print(i)
            percentage=round((i/totalEntries)*100)
            #print(percentage)
            if(i == percentagePrintFlag):
                #print("Entrou")
                percentage += j
                print(f'{percentage}%')
                i=0
                j +=1
            #if(i==100):
            #    break
        print("Importation complete!")
        print("Number of new nominatim database entries:", i)
        os.system("nominatim index")
        searchRankUpdate(curr, conn)
        curr.close()
        conn.close()
    except (Exception, DatabaseError) as error:
        print(error)

if __name__ == '__main__':
    # command line arguments (ex.: python3 script.py DbName UserName Password)
    n = len(sys.argv)
    for i in range(1, n):
        if(i == 1):
            dbname = sys.argv[i]
        if (i == 3):
            password = sys.argv[i]
        if(i == 2):
            user = sys.argv[i]

    # initializing postgresql server
    if(n==4):
        connString = "dbname={} user={} password={}"
        connString = connString.format(dbname, user, password)
    else:
        connString = "dbname={} user={}"
        connString = connString.format(dbname, user)
    os.system("sudo service postgresql start")
    insertScript()

