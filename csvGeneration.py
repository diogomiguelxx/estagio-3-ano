from distutils.util import execute
from psycopg2 import *
import os
import sys

def csvGen(connString):
    print("Connect to the PostgreSQL database server") 
    try:
        # connect to the PostgreSQL server
        print("Connecting to the PostgreSQL database...")
        
        conn = connect(connString)
        
        
        # exporting to csv
        cur = conn.cursor()
        print("Exporting data do csv file...")
        
        # running sql query remotely       
        sql_query = "COPY (SELECT centroid, nome_completo, CONCAT(cp4, '-', cp3) as cp FROM ctt_cp7 JOIN ctt_morada ON (ctt_morada.cp7_id = ctt_cp7.id) WHERE centroid IS NOT NULL) TO '{path}/search_data.csv' WITH CSV DELIMITER ',' HEADER ENCODING 'LATIN9';"
        sql_query = sql_query.format(path = os.getcwd())
        cur.execute(sql_query)
        
        print("Exporting complete!")
        
        
        #closing remaining connections
        cur.close()
        conn.close()
        
    except (Exception, DatabaseError) as error:
        print(error)
    
    print("Database connection closed.")

if __name__ == '__main__':
    # command line arguments (ex.: python3 import.py DbName UserName Password)
    n = len(sys.argv)
    
    for i in range(1, n):
        if(i == 1):
            dbname = sys.argv[i]
        if (i == 3):
            password = sys.argv[i]
        if(i == 2):
            user = sys.argv[i]
    if(len(sys.argv)==4):
        connString = "dbname={} user={} password={}"
        connString = connString.format(dbname, user, password)
    else:
        connString = "dbname={} user={}"
        connString = connString.format(dbname, user)
    
    os.system("sudo service postgresql start")
    csvGen(connString)