from distutils.util import execute
from psycopg2 import *
import os
import sys

def csvGen(connString):
    print("Connect to the PostgreSQL database server") 
    try:
        # connect to the PostgreSQL server
        print("Connecting to the PostgreSQL database...")
        
        conn = connect(connString)
        
        
        # exporting to csv
        cur = conn.cursor()
        print("Exporting data do csv file...")
        
        # running sql query remotely       
        sql_query = "COPY (SELECT CONCAT(cp4, '-', cp3) as postcode, ST_y(ST_AsText(ST_centroid(centroid))) as lat, ST_x(ST_AsText(ST_centroid(centroid))) as lon FROM ctt_cp7 WHERE ST_X(ST_AsText(ST_centroid(centroid))) IS NOT NULL AND  ST_y(ST_AsText(ST_centroid(centroid))) IS NOT NULL) TO '{path}/pt_postcodes.csv' WITH CSV DELIMITER ',' HEADER;"
        sql_query = sql_query.format(path = os.getcwd())
        cur.execute(sql_query)
        
        print("Exporting complete!")
        
        # now that we have the csv file with all the data, run pcExport()
        
        pcExport()
        
        #closing remaining connections
        cur.close()
        conn.close()
        
    except (Exception, DatabaseError) as error:
        print(error)
    
    print("Database connection closed.")

def pcExport():
    print("Inserting the new data...")
    # inserting the csv data into the right table
    os.system("nominatim refresh --postcodes")
    print("Reindexing tables...")
    # reindexing the remaining tables with the needed data
    os.system("nominatim index")
    print("New data imported with sucess")


if __name__ == '__main__':
    # command line arguments (ex.: python3 import.py DbName UserName Password)
    n = len(sys.argv)
    
    for i in range(1, n):
        if(i == 1):
            dbname = sys.argv[i]
        if (i == 3):
            password = sys.argv[i]
        if(i == 2):
            user = sys.argv[i]
    if(len(sys.argv)==4):
        connString = "dbname={} user={} password={}"
        connString = connString.format(dbname, user, password)
    else:
        connString = "dbname={} user={}"
        connString = connString.format(dbname, user)
    
    os.system("sudo service postgresql start")
    csvGen(connString)
    
